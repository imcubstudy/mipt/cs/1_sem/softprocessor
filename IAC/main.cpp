#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cassert>
#include <cmath>
#include <map>
#include <string>
#include <climits>
#include "commands.h"

struct jmp_point_t
{
    int point 	= POISON_JMP_P;
};

long    fileSize( FILE* file );
char*   get_src( char src_file_name[], long* src_size );

int main(int argc, char* argv[] )
{
	assert( argc == 3 );

    long    src_size  = -1;
    char*   src       = get_src( argv[1], &src_size );

    void*   exe       = calloc( sizeof( double ) + sizeof( char ), src_size );

    std::map< std::string, jmp_point_t > labels;

    FILE*   exe_file        = fopen( argv[2], "wb" );

	char 	command_n		= '\0';

    bool    second_way      = false;

    long    curpos_src      = 0;
    void*   curpos_exe      = exe;
    long    curcom          = 0;
	while( curpos_src < src_size - 1 )
    {
        curcom++;
        char    command_name[$MAX_COMMAND_NAME_LEN] = {};
        int     char_readen = -1;
        sscanf( src + curpos_src, "%s%n", command_name, &char_readen );
        curpos_src += char_readen;

        if( false ){ ; }

        #define DEF_CMD( name, number, iacomp_code, icpu_code )\
            iacomp_code

        	#include "commands.h"

        #undef DEF_CMD

        else
        {
            char possible_error[MAX_ERRMSG_LEN] = {};
            sprintf( possible_error, "syntax error command %s (%ld)", command_name, curcom );
            printf( "%s\n", possible_error );
            abort();
        }
    }

    second_way = true;

    curpos_src = 0;
    curpos_exe = exe;
    curcom     = 0;
	while( curpos_src < src_size - 1 )
    {
        curcom++;
        char    command_name[$MAX_COMMAND_NAME_LEN] = {};
        int     char_readen = -1;
        sscanf( src + curpos_src, "%s%n", command_name, &char_readen );
        curpos_src += char_readen;

        if( false ){ ; }

        #define DEF_CMD( name, number, iacomp_code, icpu_code )\
            iacomp_code

        	#include "commands.h"

        #undef DEF_CMD

        else
        {
            char possible_error[MAX_ERRMSG_LEN] = {};
            sprintf( possible_error, "%s %ld", "Syntax error, command = ", curcom );
            printf( "%s\n", possible_error );
            abort();
        }
    }

    fwrite( &$LANG_NAME, sizeof( char ), $LANG_NAME_LEN, exe_file );
    int LANG_VER = $LANG_VERS;
    fwrite( &LANG_VER,   sizeof( int ),  1,              exe_file );
    fwrite( exe,         sizeof( char ), (long)curpos_exe - (long)exe, exe_file);
    
	fclose( exe_file );
    free( src );
	free( exe );

    printf( "Success!\n" );

    return 0;
}


long fileSize( FILE* file )
{
    assert( nullptr != file );
    assert( ferror( file ) == 0 );

    long currentPosition = ftell( file );

    fseek( file, 0, SEEK_END );
    long size  = ftell( file );

    fseek( file, currentPosition, SEEK_SET );

    return size;
}

char* get_src( char src_file_name[], long* src_size )
{
    FILE*   src_file = fopen( src_file_name, "r" );
    *src_size        = fileSize( src_file );
    char*   src      = (char*)calloc( sizeof( char ), *src_size );
    fread( src, *src_size, sizeof( char ), src_file );
    fclose( src_file );
    return src;
}


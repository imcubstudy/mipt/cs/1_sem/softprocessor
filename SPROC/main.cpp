#include "containers.h"
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include "commands.h"

using std::max;

using namespace vl_containers;
#define stack_t vl_containers::stack_t

struct ICPU_t
{
    double    		poison_data;
    int       		poison_comeback;

    double*         ram;
    int             ram_size;
    
	double          regs[NUM_OF_REGS];
    
	stack_t*        data;
    stack_t*        comeback;	 
};

void run_program( ICPU_t* icpu, void* program )
{
    assert( program != nullptr );
	assert( icpu );

    bool    end     = false;

    long    curpos  = 0;

    while( !end )
    {
        char cm = -1;
        memcpy( &cm, (char*)program + curpos, sizeof( char ) );
        curpos += sizeof( char );
        switch( cm )
        {
            #define DEF_CMD( name, number, iacomp_code, icpu_code )\
                icpu_code
	            #include "commands.h"

            #undef DEF_CMD

            default:
            {
                assert( !"Woof!" );
            }
        }
    }
}

ICPU_t* construct_ICPU()
{
	ICPU_t* 	icpu = (ICPU_t*)calloc( sizeof( ICPU_t ), 1 );
	assert( icpu && "Can't alloc icpu" );

	icpu->poison_data 		= POISON_DATA;
	icpu->poison_comeback 	= POISON_COMEBACK;
	icpu->ram_size			= DEF_RAM_SIZE;

    icpu->comeback = construct_stack( 	"comeback", 
										sizeof( int ), 
										int_default_funcs, 
										&(icpu->poison_comeback), 
										STK_DEFAULT_CAPACITY );
	assert( icpu->comeback && "Can't constuct comeback stack" );

    icpu->data     = construct_stack( 	"data", 
										sizeof( double ), 
										double_default_funcs, 
										&(icpu->poison_data), 
										STK_DEFAULT_CAPACITY );
	assert( icpu->data && "Can't construct data stack" );

    icpu->ram = (double*)calloc( sizeof( double ), icpu->ram_size );
	assert( icpu->ram && "Can't alloc ram" );
	
	return icpu;
}

void destruct_ICPU( ICPU_t* icpu )
{
	destruct_stack( icpu->comeback );
	destruct_stack( icpu->data );
	free( icpu->ram );
	free( icpu );
}

long fileSize( FILE* file )
{
    assert( nullptr != file );
    assert( ferror( file ) == 0 );

    long currentPosition = ftell( file );

    fseek( file, 0, SEEK_END );
    long size  = ftell( file );

    fseek( file, currentPosition, SEEK_SET );

    return size;
}

void* create_program( char exe_filename[] )
{
	assert( exe_filename );

    FILE* exe   = fopen( exe_filename, "rb" );
    assert( exe );
	
	char  lang[$LANG_NAME_LEN] 	= {};
    int   ver           		= -1;

    fread( lang, sizeof( char ), $LANG_NAME_LEN, exe );
    fread( &ver, sizeof( int ), 1, exe );

    if( strcmp( lang, $LANG_NAME ) != 0 || ver != $LANG_VERS )
        assert( !"Signature failed" );

    long 	file_size 	= fileSize( exe ) - ftell( exe );
    void* 	program	 	= calloc( 1, file_size );
    assert( program );

	fread( program, 1, file_size, exe );

    return program;
}

int main( int argc, char* argv[] )
{
	assert( argc == 2 );

    ICPU_t* icpu = construct_ICPU();
	assert( icpu );

    void* program = create_program( argv[1] );
	
    run_program( icpu, program );

    free( program );	
	destruct_ICPU( icpu );

    return 0;
}


#ifndef	__CPU_PARAMS__
#define __CPU_PARAMS__

	#define POISON_DATA				NAN
	#define	POISON_JMP_P			INT_MAX
	#define POISON_COMEBACK			INT_MAX
	#define	DEF_RAM_SIZE			100
	#define	NUM_OF_REGS				4
	
	#define $LANG_NAME          	"imcode"
	#define $LANG_VERS          	3
	#define $LANG_NAME_LEN      	10

	#define $MAX_COMMAND_NAME_LEN   10

#endif // __CPU_PARAMS__

#define MAX_ERRMSG_LEN          1000
#define MAX_JMP_LABEL_NAME_LEN  1000

#define END_N                   0
#define MUL_N                   1
#define SUB_N                   2
#define DIV_N                   3
#define ADD_N                   4
#define SQRT_N                  5
#define SIN_N                   6
#define COS_N                   7
#define IN_N                    8
#define OUT_N                   9
#define LABEL_N                 10
#define PUSH_N                  11
#define PUSHR_N                 12
#define PUSHRam_N               13
#define PUSHRamR_N              14
#define POP_N                   15
#define POPRam_N                16
#define POPRamR_N               17
#define JMP_N                   18
#define JE_N                    19
#define JNE_N                   20
#define JA_N                    21
#define JAE_N                   22
#define JB_N                    23
#define JBE_N                   24
#define CALL_N                  25
#define RET_N                   26

#define CHECK_REG( inp_rn, reg_number )															\
    if( inp_rn == reg_number )																	\
        reg = reg_number;

#define CHECK_REGS( name )																		\
	int inp_reg = name;																			\
	for( int i = 1; i <= NUM_OF_REGS; ++i )														\
    	CHECK_REG( inp_reg, i );																\
    if( reg == -1 )																				\
    {																							\
        char error_string[MAX_ERRMSG_LEN] = {};													\
        sprintf( error_string, "%s %ld", "Unknown reg in src command = ", curcom );				\
        printf( "%s\n", error_string );															\
        abort();																				\
    }

#define WRITE_EXE( type, src )																	\
    memcpy( curpos_exe, &src, sizeof( type ) );													\
    curpos_exe = (char*)curpos_exe + sizeof( type );

#define SIMPLE_COMMAND_IAC_CODE( name )															\
    else if( strcmp( command_name, #name ) == 0 )												\
    {																							\
        command_n = name##_N;																	\
        WRITE_EXE( char, command_n );															\
    }

#define REG_COMMAND_IAC_SUBCODE( name, number )													\
    if( sscanf( src + curpos_src, "%*[ \t]r%d%n", &name##_arg_r, &char_readen ) >= 1 )			\
    {																							\
        curpos_src += char_readen;																\
        																						\
        int reg = -1;																			\
        CHECK_REGS( name##_arg_r );																\
       	command_n = number;																		\
        WRITE_EXE( char, command_n );															\
        																						\
        WRITE_EXE( int, reg );																	\
    }

#define BIN_COMMAND_ICPU_CODE( number, oper )													\
    case number:																				\
    {																							\
        double a = NAN, b = NAN;																\
        assert( stk_pop( icpu->data, &b ) != EMPTY_POP && number );								\
        assert( stk_pop( icpu->data, &a ) != EMPTY_POP && number );								\
        double c = a oper b;																	\
        assert( stk_push( icpu->data, &c ) == NO_ERROR && number );								\
        break;																					\
    }

#define UN_COMMAND_ICPU_CODE( number, func )													\
    case number:																				\
    {																							\
        double t = NAN;																			\
        assert( stk_pop( icpu->data, &t ) != EMPTY_POP && number );									\
        t = func( t );																			\
        assert( stk_push( icpu->data, &t ) == NO_ERROR && number );									\
        break;																					\
    }

#define JMP_IAC_CODE( name )																	\
    else if( strcmp( command_name, #name ) == 0 )												\
    {																							\
        char jmp_label_name[MAX_JMP_LABEL_NAME_LEN] = {};										\
        if( sscanf( src + curpos_src, "%s%n", jmp_label_name, &char_readen ) >= 1 )				\
        {																						\
            curpos_src += char_readen;															\
            command_n = name##_N;																\
            WRITE_EXE( char, command_n );														\
            																					\
            std::string jmp_label_name_string( jmp_label_name );								\
            int jmp_point = labels[jmp_label_name_string].point;								\
            if( second_way && jmp_point == POISON_JMP_P ) assert( !"Undefined jump point" );	\
            WRITE_EXE( int, jmp_point );														\
        }																						\
        else assert( !"Where should i jump?" );													\
    }

#define COND_JMP_ICPU_CODE( number, cond )														\
    case number :																				\
        {																						\
            double a = NAN, b = NAN;															\
            assert( stk_pop( icpu->data, &b ) != EMPTY_POP && number );							\
            assert( stk_pop( icpu->data, &a ) != EMPTY_POP && number );						    \
            																					\
            bool jump       = a cond b;															\
            int  jmp_point  = -1;																\
            memcpy( &jmp_point, (char*)program + curpos, sizeof( int ) );					    \
            curpos += sizeof( int );															\
            																					\
            if( jump ) 																			\
			{																					\
				if( jmp_point != POISON_JMP_P )													\
					curpos = jmp_point;															\
				else assert( !"POISON JMP POINT" );												\
			}																					\
            break;																				\
        }

#define DEF_COND_JMP( name, number, cond )														\
    DEF_CMD( name, number, JMP_IAC_CODE( name ), COND_JMP_ICPU_CODE( number, cond ) )

#define DEF_SIMPLE_BIN_CMD( name, number, oper )												\
    DEF_CMD( name, number, SIMPLE_COMMAND_IAC_CODE( name ), BIN_COMMAND_ICPU_CODE( number, oper ) )

#define DEF_SIMPLE_UN_CMD( name, number, func )													\
    DEF_CMD( name, number,  SIMPLE_COMMAND_IAC_CODE( name ), UN_COMMAND_ICPU_CODE( number, func ) )

#ifdef DEF_CMD

#ifdef __VL_CONTAINERS_H__

using namespace vl_containers;

#endif

    DEF_SIMPLE_BIN_CMD( MUL, MUL_N, * )

    DEF_SIMPLE_BIN_CMD( SUB, SUB_N, - )

    DEF_SIMPLE_BIN_CMD( DIV, DIV_N, / )

    DEF_SIMPLE_BIN_CMD( ADD, ADD_N, + )

    DEF_SIMPLE_UN_CMD( SQRT, SQRT_N, sqrt )

    DEF_SIMPLE_UN_CMD( SIN, SIN_N, sin )

    DEF_SIMPLE_UN_CMD( COS, COS_N, cos )

    DEF_CMD( END,  END_N,  SIMPLE_COMMAND_IAC_CODE( END ),

            case END_N:
            {
                end = true;
                break;
            }
    )

    DEF_CMD( IN, IN_N, SIMPLE_COMMAND_IAC_CODE( IN ),

            case IN_N:
            {
                printf( "Awaiting input: " );
                double t = NAN;
                scanf( "%lg", &t );
                assert( stk_push( icpu->data, &t ) == NO_ERROR && IN_N );
                break;
            }
    )

    DEF_CMD( OUT,  OUT_N, SIMPLE_COMMAND_IAC_CODE( OUT ),

            case OUT_N:
            {
                double t = NAN;
                assert( stk_top( icpu->data, &t ) != EMPTY_POP && OUT_N );
                printf( "%lg\n", t );
                break;
            }
    )

    DEF_CMD( LABEL, LABEL_N,
        else if( strcmp( command_name, "LABEL" ) == 0 )
        {
            assert( LABEL_N != PUSHR_N );

            command_n = LABEL_N;

            char label_name[MAX_JMP_LABEL_NAME_LEN] = {};
            if( sscanf( src + curpos_src, "%s%n", label_name, &char_readen ) >= 1 )
            {
                curpos_src += char_readen;
                std::string label_name_string( label_name );

                if( !second_way && labels[label_name_string].point != POISON_JMP_P )   
				{
					printf( "Double label declaration, label = %s\n", label_name );
					abort();
				}
                if(  second_way && labels[label_name_string].point == POISON_JMP_P )
				{
					assert( !"Something went wrong" );
				}
                labels[label_name_string].point = (long)curpos_exe - (long)exe;
            } else assert( !"Label err" );
        }, )

    DEF_CMD( PUSH, PUSH_N,

            else if( strcmp( command_name, "PUSH" ) == 0 )
            {
                assert( PUSH_N != PUSHR_N );

                double  PUSH_arg_d     	 = NAN;
                int	    PUSH_arg_r   	 = -1;
                if( sscanf( src + curpos_src, "%lg%n", &PUSH_arg_d, &char_readen ) >= 1 )
                {
                    curpos_src += char_readen;
                    command_n = PUSH_N;

                    WRITE_EXE( char, command_n );

                    WRITE_EXE( double, PUSH_arg_d );
                }
                else REG_COMMAND_IAC_SUBCODE( PUSH, PUSHR_N )
                else if( sscanf( src + curpos_src, "%*[ \t][%lg]%*c%n", &PUSH_arg_d, &char_readen ) >= 1 )
                {
                    curpos_src += char_readen;
                    command_n = PUSHRam_N;

                    WRITE_EXE( char, command_n );

                    WRITE_EXE( double, PUSH_arg_d );
                }
                else if( sscanf( src + curpos_src, "%*[ \t][r%d]%*c%n", &PUSH_arg_r, &char_readen ) >= 1 )
                {

                    curpos_src += char_readen;
                    command_n = PUSHRamR_N;

                    WRITE_EXE( char, command_n );

                    int reg = -1;
                    CHECK_REGS( PUSH_arg_r );

                    WRITE_EXE( int, reg );
                }
            }
        ,

            case PUSH_N:
            {
                double t = NAN;
                memcpy( &t, (char*)program + curpos, sizeof( double ) );
                curpos += sizeof( double );
                assert( stk_push( icpu->data, &t ) == NO_ERROR && PUSH_N );
                break;
            }
            case PUSHR_N:
            {
                int reg = -1;
                memcpy( &reg, (char*)program + curpos, sizeof( int ) );
                curpos += sizeof( int );
                assert( stk_push( icpu->data, &(icpu->regs)[reg] ) == NO_ERROR && PUSHR_N );
                break;
            }
            case PUSHRam_N:
            {
                double t = NAN;
                memcpy( &t, (char*)program + curpos, sizeof( double ) );
                curpos += sizeof( double );

                assert( (int)t < icpu->ram_size && PUSHRam_N );

                memcpy( &t, (icpu->ram) + (int)t, sizeof( double ) );

                assert( stk_push( icpu->data, &t ) == NO_ERROR && PUSHRam_N );

                break;
            }
            case PUSHRamR_N:
            {
                int reg = -1;
                memcpy( &reg, (char*)program + curpos, sizeof( int ) );
                curpos += sizeof( int );

                assert( (int)(icpu->regs)[reg] < icpu->ram_size && PUSHRamR_N );

                double t = NAN;
                memcpy( &t, (icpu->ram) + (int)(icpu->regs)[reg], sizeof( double ) );

                assert( stk_push( icpu->data, &t ) == NO_ERROR && PUSHRamR_N );

                break;
            }
    )

    DEF_CMD( POP, POP_N,

            else if( strcmp( command_name, "POP" ) == 0 )
            {
                assert( POP_N != PUSHR_N );

                int	    POP_arg_r      = -1;
                double  POP_arg_d      = NAN;

                REG_COMMAND_IAC_SUBCODE( POP, POP_N )
                else if( sscanf( src + curpos_src, "%*[ \t][r%d]%*c%n", &POP_arg_r, &char_readen ) >= 1 )
                {

                    curpos_src += char_readen;
                    command_n = POPRamR_N;

                    WRITE_EXE( char, command_n );

                    int reg = -1;
                    CHECK_REGS( POP_arg_r );

                    WRITE_EXE( int, reg );
                }
                else if( sscanf( src + curpos_src, "%*[ \t][%lg]%*c%n", &POP_arg_d, &char_readen ) >= 1 )
                {
                    curpos_src += char_readen;
                    command_n = POPRam_N;

                    WRITE_EXE( char, command_n );

                    WRITE_EXE( double, POP_arg_d );
                }

            }
        ,

            case POP_N:
            {
                int reg = -1;
                memcpy( &reg, (char*)program + curpos, sizeof( int ) );
                curpos += sizeof( int );
                assert( stk_pop( icpu->data, &(icpu->regs)[reg] ) != EMPTY_POP && POP_N );
                break;
            }
            case POPRamR_N:
            {
                int reg = -1;
                memcpy( &reg, (char*)program + curpos, sizeof( int ) );
                curpos += sizeof( int );

                if( (int)(icpu->regs)[reg] >= icpu->ram_size )
                {
					assert( !"Ram buffer owerflow" );
                }

                assert( stk_pop( icpu->data, &(icpu->ram)[(int)(icpu->regs)[reg]] ) != EMPTY_POP && POPRam_N );
                break;
            }
            case POPRam_N:
            {
                double t = NAN;
                memcpy( &t, (char*)program + curpos, sizeof( double ) );
                curpos += sizeof( double );

                if( (int)t >= icpu->ram_size )
                {
					assert( !"Ram buffer owerflow" );
                }

                assert( stk_pop( icpu->data, (icpu->ram) + (int)t ) == NO_ERROR && POPRam_N );

                break;
            }
    )

    DEF_CMD( JMP, JMP_N, JMP_IAC_CODE( JMP ),
            case JMP_N:
            {
                int jmp_point = -1;
                memcpy( &jmp_point, (char*)program + curpos, sizeof( int ) );
				if( jmp_point == POISON_JMP_P ) assert( !"POISON JMP POINT" );
                curpos = jmp_point;
                break;
            }
    )

    DEF_COND_JMP( JE, JE_N, == )

    DEF_COND_JMP( JNE, JNE_N, != )

    DEF_COND_JMP( JA, JA_N, > )

    DEF_COND_JMP( JAE, JAE_N, >= )

    DEF_COND_JMP( JB, JB_N, < )

    DEF_COND_JMP( JBE, JBE_N, <= )

    DEF_CMD( CALL, CALL_N, JMP_IAC_CODE( CALL ),
        case CALL_N :
        {
            int jmp_point = -1;
            memcpy( &jmp_point, (char*)program + curpos, sizeof( int ) );

            curpos += sizeof( int );

            assert( stk_push( icpu->comeback, &curpos ) == NO_ERROR && CALL_N );
			if( jmp_point == POISON_JMP_P ) assert( !"POISON JMP POINT" );
            curpos = jmp_point;

            break;
        }
    )

    DEF_CMD( RET, RET_N, SIMPLE_COMMAND_IAC_CODE( RET ),
        case RET_N :
        {
            int ret_point = INT_MAX;

            assert( stk_pop( icpu->comeback, &ret_point ) != EMPTY_POP );
            curpos = ret_point;

            break;
        }
    )

#endif

#undef END_N
#undef MUL_N
#undef SUB_N
#undef DIV_N
#undef ADD_N
#undef SQRT_N
#undef SIN_N
#undef COS_N
#undef IN_N
#undef OUT_N
#undef LABEL_N
#undef PUSH_N
#undef PUSHR_N
#undef POP_N
#undef JMP_N
#undef JMP_N
#undef JE_N
#undef JNE_N
#undef JA_N
#undef JAE_N
#undef JB_N
#undef JBE_N
#undef CALL_N
#undef RET_N

#undef JMP_IAC_CODE
#undef DEF_COND_JMP
#undef COND_JMP_ICPU_CODE

#undef SIMPLE_COMMAND_IAC_CODE
#undef BIN_COMMAND_ICPU_CODE

#undef DEF_SIMPLE_BIN_CMD
#undef DEF_SIMPLE_UN_CMD

#undef CHECK_REG
#undef CHECK_REGS
#undef REG_COMMAND_IAC_SUBCODE

#undef MAX_JMP_LABEL_NAME_LEN
#undef WRITE_EXE
